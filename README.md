# README #

This represents an attempt to set up a Relay-powered isomorphic app that shares part of the codebase with a React Native app (iOS and Android). 

The idea behind this small "boilerplate" is to demonstrate one of the ways to share code between native and web.

The two apps share the same Relay server; all of the components, with the exception of the main container, share the same logic and import a `Render` file that contains the view. 

Separate views are necessary as the native components are oblivious to `<div>`, `<p>` and other tags, and use their own native components instead (`<View>`, `<Text>`...).

This is what the app looks like:

![75.png](https://bitbucket.org/repo/za8byR/images/1586940310-75.png)

### How it works ###

Some articles are fetched as a sample through a script, then a mock database is built with some helper functions. The GraphQL schema is then generated and the data served by Relay to the two main containers: 

* `AppContainer.native.js`
* `AppContainer.web.js`

Entry points:

* `/src/app.js` is the entry point for native;
* `/src/client.js` is the entry point for the client;
* `/src/server.js` is the entry point for the server;

### Setup ###
You need to have a [native environment development setup](https://facebook.github.io/react-native/docs/getting-started.html) ready, possibly with Genymotion for Android (it's the quickest way to get up and running).

#####Web:

* `npm install`
* `webpack`
* `npm run start-relay`

Keep the Relay server running!

#####Native:
* `npm start` to start the React Native packager

Keep the packager running!

######iOS
* `npm run ios` to compile for iOS
* open `ios/financialTimes.xcodeproj` with Xcode and build

######Android:
If you want to use Genymotion (advised), replace localhost with your ip address (ifconfig -> en0 --> inet) [here](https://bitbucket.org/ericat/ft-isomorph/src/67596906f3554d0db5596e7ac901964722f30a75/src/app.js?at=master&fileviewer=file-view-default#app.js-17).

* launch a simulator instance with Genymotion
* `npm run ad` to compile and launch Android

### Debugging ###
Issues in React Native as usually solved by clearing the cache and rebuilding;  the below commands may be useful:

* `npm start-cache` to start the RN cli with a clean cache;
* `rm -fr $TMPDIR/react-* && watchman watch-del-all`