'use strict';
/*global window: false */

import React, { Component } from 'react';
window.React = React;

import Relay, {
  DefaultNetworkLayer,
  RootContainer,
} from 'react-relay';

import AppContainerNative from './containers/AppContainer.native';
import AppHomeRoute from './routes/AppHomeRoute';

// replace localhost with your current ip for genymotion
Relay.injectNetworkLayer(
  new DefaultNetworkLayer('http://localhost:8080/graphql')
);

export default class HomePageFeedApp extends Component {
  render(): void {
    return (
      <RootContainer
        Component={AppContainerNative}
        route={new AppHomeRoute()}
      />
    );
  }
}

