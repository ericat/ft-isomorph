export class Newsfeed {}
export class Article {}

const newsfeed = new Newsfeed();
newsfeed.id = 1;

const data = require('../../data.json');
const articlesJson = data.pageItems;

const articles = articlesJson.map(item => {
  let article = new Article();
  article.id = item.id;
  article.title = item.title.title;
  article.author = item.editorial.byline;
  article.url = item.location.uri;
  article.subheading = item.editorial.subheading;
  article.teaser = item.editorial.standFirst;
  article.image = item.images.length > 0 ?
    item.images[0].url :
    'http://im.ft-static.com/content/images/d0260fac-b520-46c5-94bf-b080947ead55.img';
  return article;
});

export function getNewsfeed() { return newsfeed; }
export function getArticles() { return articles; }
export function getArticle(id) {
  return articles.find(article => article.id === id);
}


