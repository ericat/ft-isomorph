import Relay from 'react-relay';
import React from 'react';
import {
    View,
    ListView,
    ScrollView,
} from 'react-native';

import ArticleTeaser from '../components/ArticleTeaser/Component';
import Toolbar from '../components/Toolbar/Component';

const styles = require('../components/styles.native').styles;

class AppContainerNative extends React.Component {
  static propTypes = {
    newsfeed: React.PropTypes.shape({
      articles: React.PropTypes.shape({
        edges: React.PropTypes.arrayOf(
          React.PropTypes.shape({
            node: React.PropTypes.shape({
              id: React.PropTypes.string,
              title: React.PropTypes.string,
              author: React.PropTypes.string,
              subheading: React.PropTypes.string,
              teaser: React.PropTypes.string,
              image: React.PropTypes.string,
            }).isRequired,
          }).isRequired
        ).isRequired,
      }).isRequired,
    }).isRequired,
    relay: React.PropTypes.shape({
      variables: React.PropTypes.shape({
        count: React.PropTypes.number,
      }),
      setVariables: React.PropTypes.func,
    }).isRequired,
  };
  constructor(props, context) {
    super(props, context);

    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  }

  componentWillMount() {
    const articles = this.props.newsfeed.articles.edges;

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(articles),
    });
  }

  componentWillReceiveProps(nextProps) {
    const nextArticleTeasers = nextProps.newsfeed.articles.edges;
    const currentArticleTeasers = this.props.newsfeed.articles.edges;

    if (nextArticleTeasers !== currentArticleTeasers) {
      const newDataSource = this
      .state
      .dataSource
      .cloneWithRows(nextArticleTeasers);

      this.setState({ dataSource: newDataSource });
    }
  }

  _renderArticleTeaser(article) {
    const articleBody = article.node;

    return (
      <ArticleTeaser
        article={articleBody}
        styles={styles}
      />
    );
  }

  render() {
    return (
      <ScrollView stickyHeaderIndices={[0]}>
        <Toolbar
          styles={styles}
        />
        <View style={styles.mainContainer}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this._renderArticleTeaser.bind(this)}
        />
        </View>
      </ScrollView>
    );
  }
}

export default Relay.createContainer(AppContainerNative, {
  initialVariables: {
    count: 10,
  },
  fragments: {
    newsfeed: () => Relay.QL`
      fragment on Newsfeed {
        articles(first: $count) {
          edges {
            node {
              id,
              title,
              author,
              subheading,
              teaser,
              image,
            },
            cursor,
          },
          pageInfo {
            hasPreviousPage,
            hasNextPage
          }
        },
      }
    `,
  },
});

