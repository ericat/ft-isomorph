export default {
  navBar: {
    position: 'fixed',
    backgroundColor: '#424242',
    color: '#FFF',
    width: '100%',
    height: 77,
  },
  imgContainer: {
    minWidth: 274,
  },
  articleContainer: {
    display: 'flex',
    margin: '30px 0 30px 0',
  },
  rightContainer: {
    marginLeft: 20,
  },
  paragraph: {
    margin: '0 10px 10px 0',
  },
  mainHeader: {
    fontSize: 20,
  },
  author: {
    color: '#9e2f50',
    fontFamily: 'sans-serif',
    fontWeight: 500,
    fontSize: 15,
  },
  subHeading: {
    fontFamily: 'sans-serif',
  },
  teaser: {
    lineHeight: '22px',
  },
};
