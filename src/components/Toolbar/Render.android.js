import React from 'react';
import {
  View,
  ToolbarAndroid,
} from 'react-native';


export default function render(options = {}) {
  const { styles } = options;
  const title = 'Financial Times'.toUpperCase();

  return(
    <View>
      <ToolbarAndroid
        logo={require('../../../public/img/logo.png')}
        style={styles.toolbar}
        subtitle='UK Edition'
        title={title}
      />
    </View>
  );
}



