import React from 'react';

export default function render(options = {}) {
  const { styles } = options;
  const main = 'Financial Times'.toUpperCase();

  return(
    <div style={styles.navBar}>
      <h1>{main}</h1>
    </div>
  );
}


