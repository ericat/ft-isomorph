import React from 'react';
import {
  Text,
  View,
} from 'react-native';

export default function render(options = {}) {
  const { styles } = options;
  const title = 'Financial Times'.toUpperCase();

  return(
    <View style={[styles.toolbar, styles.toolbarIos]}>
      <Text style={styles.headerIos}>{title}</Text>
    </View>
  );
}


