import React from 'react';
import Render from './Render';

class Toolbar extends React.Component {
  static propTypes = {
    styles: React.PropTypes.shape({}),
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const options = {
      styles: this.props.styles,
    };

    return Render.bind(this)(options);
  }
}

export default Toolbar;
