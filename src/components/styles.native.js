import {
  StyleSheet,
} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#fff1e0',
  },
  whiteColor: {
    color: '#FFF',
    fontWeight: '200',
  },
  articleContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#E7E7E7',
    paddingTop: 10,
    paddingBottom: 10,
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  rightContainer: {
    flex: 1,
    alignSelf: 'center',
  },
  title: {
    fontFamily: 'Helvetica',
    textAlign: 'left',
    marginBottom: 4,
    marginLeft: 5,
  },
  rubric: {
    fontFamily: 'verdana',
  },
  author: {
    color: '#9e2f50',
  },
  image: {
    width: 200,
    height: 200,
  },
  thumbnail: {
    width: 85,
    height: 65,
    alignSelf: 'center',
    marginLeft: 5,
  },
  toolbar: {
    backgroundColor: '#BDBDBD',
    height: 66,
    borderColor: '#9e2f50',
    borderBottomWidth: 5,
  },
  toolbarIos: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingBottom: 10,
    backgroundColor: '#fff1e0',
    height: 76,
    alignItems: 'center',
  },
  headerIos: {
    fontSize: 25,
    fontFamily: 'georgia',
  },
});

