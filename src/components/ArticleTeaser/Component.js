import React from 'react';
import Render from './Render';

class ArticleTeaser extends React.Component {
 static propTypes = {
    article: React.PropTypes.shape({
      id: React.PropTypes.string.isRequired,
      title: React.PropTypes.string,
      author: React.PropTypes.string,
      subheading: React.PropTypes.string,
      teaser: React.PropTypes.string,
      image: React.PropTypes.string,
    }).isRequired,
    styles: React.PropTypes.shape({}),
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const options = {
      styles: this.props.styles,
      article: this.props.article,
    };

    return Render.bind(this)(options);
  }
}

export default ArticleTeaser;
