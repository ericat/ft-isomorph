import React from 'react';
import {
  Text,
  View,
  Image,
} from 'react-native';

function render(options = {}) {
  const { article, styles } = options;

  return (
    <View style={styles.articleContainer}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{article.title}</Text>
        <Text>{' | '}</Text>
        <Text style={[styles.rubric, styles.author]}>
          {article.author}
        </Text>
      </View>
      <View style={styles.titleContainer}>
        <Image
          source={{ uri: article.image }}
          style={styles.thumbnail}
        />
        <View style={styles.rightContainer}>
          <Text style={styles.title}>{article.subheading}</Text>
          <Text style={[styles.title, styles.rubric]}>{article.teaser}</Text>
        </View>
      </View>
  </View>
  );
}

module.exports = render;
