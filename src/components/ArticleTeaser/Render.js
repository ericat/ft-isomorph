import React from 'react';

export default function render(options = {}) {
  const { article, styles } = options;

  return(
    <div style={styles.articleContainer}>
    <div style={styles.imgContainer}>
      <img src={article.image} />
    </div>
    <div style={styles.rightContainer}>
      <p style={styles.paragraph}>
        <span style={styles.mainHeader}>{article.title}</span>
        <span>{' | '}</span>
        <span style={styles.author}>
          {article.author}
        </span>
      </p>
      <p style={Object.assign(styles.paragraph, styles.subHeading)}>
        {article.subheading}
      </p>
      <p style={styles.teaser}>{article.teaser}</p>
    </div>
    </div>
  );
}

