var fs = require('fs');
var path = require('path');
var wreck = require('wreck');
var config = require('../config');
var mainPath = path.resolve(__dirname, '../') + '/data.json';
var url = 'http://api.ft.com/site/v1/pages/4c499f12-4e94-11de-8d4c-00144feabdc0/skyline-content?apiKey=' + config.ftApiKey;

wreck.get(url, function(err, res, payload) {
  if (err) console.error(err);
  var articles = JSON.parse(payload).pageItems;

  fs.writeFile(mainPath, payload, function(err) {
    if (err) return console.log(err);
  });
});
